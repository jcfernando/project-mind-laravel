<?php

class WindowsController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function view()
	{
	
		$users = DB::table('mind_museum_questions')->where('status','=','1')->get();

		// foreach ($users as $user)
		// {
		//     var_dump($user->name);
		// }
		
		return View::make('window.window')
			->with('users', $users);

	}





} 
