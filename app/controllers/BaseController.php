<?php

class BaseController extends Controller {

	var $indexView = 'default-index';
	var $createView = '';
	var $updateView = '';
	var $searchView = '';
	var $showView = '';
	var $storeView = '';
	var $findView = '';
	var $model = '';
	var $insertAttributes = '';
	var $otherModel = '';
	var $recordList = 'recordList';
	var $updateAttributes = '';
	var $searchAttributes = '';
	var $insertRules = [];
	var $updateRules = [];
	var $header = '';

	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	public function create()
	{
		return View::make($this->createView);
	}

	public function store()
	{
		$model = $this->model;
		$validator = Validator::make(Input::all(), $this->insertRules);

		if($validator->passes()){
			foreach($this->insertAttributes as $attrib){
					$model[$attrib] = Input::get($attrib);
			}
			$model->save();
		}
			return View::make($this->createView)->withErrors($validator);
	}

	public function index()
	{
		$data = [];
		$attributes = [];
		$recordList = $this->model->get();			//gets the values in the table
		//$data = $this->referenceData($data);
		return View::make($this->indexView)
			->with($this->recordList, $recordList)
			->with('attributes', $this->insertAttributes)
			->with('data', $data);
	}

}
