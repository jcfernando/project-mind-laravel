<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/


	var $model = '';

	var $createView = 'question.createQuestion';
	var $updateView = 'question.updateQuestion';
	var $showView = 'question.viewQuestion';
	var $indexView = 'question.storeQuestion';
	var $searchView = 'question.indexQuestion';
	var $findView = 'question.findQuestion';
	var $insertAttributes = ['question', 'name', 'school', 'contact', 'email'];
	var $searchAttributes = ['question', 'name', 'school', 'contact', 'email'];
	var $updateAttributes = ['question', 'name', 'school', 'contact', 'email'];

	var $insertRules = [];
	var $updateRules = [];


	public function __construct(Question $question){
		$this->model = $question;
		$this->insertRules = $this->model->insertRules;
		$this->updateRules = $this->model->updateRules;

	}




}
