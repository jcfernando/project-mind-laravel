<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <title> Project Mind </title>

   <!-- Bootstrap -->
   <link href="css/bootstrap.min.css" rel="stylesheet">
   <link href="css/maincss.css" rel="stylesheet">
</head>
<body>
   <div class="container-fluid">
      <div class="row">
         <!-- LEFT SIDE -->
         <div class="col-xs-9">

            <!-- FIRST ROW -->
            <div class="row">
               <div class="col-xs-10">
                  <div id="video-section">
                     Video
                  </div>
               </div>

               <div class="col-xs-2">
                  <div class="row">
                     <div class="col-xs-12">
                        <div id="date-section">
                           Date
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-xs-12">
                        <div id="time-section">
                           Time
                        </div>
                     </div>
                  </div>
               </div>
            </div>

            <!-- SECOND ROW -->
            <div class="row">
               <div class="col-xs-3">
                  <div id="idea-section">
                     Mind Moving Idea of the Day
                  </div>
               </div>



               <div class="col-xs-7">
                  <div class="row">
                     <div class="col-xs-12">
                        <div id="banner-section">
                           Banner
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-xs-12">
                        <div id="events-section">
                           Events
                        </div>
                     </div>
                  </div>
               </div>

               <div class="col-xs-2">
                  <div id="schedule-section">
                     Demo Schedules
                  </div>
               </div>

            </div>
         </div>

         <!-- RIGHT SIDE -->
         <div class="col-xs-3">
            <div id="questions-section">
               ASK A QUESTION

               @foreach($users as $user)
               
               <!-- <a href="#" data-toggle="modal" data-target="#answer-modal"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a> -->
               <!-- <div> -->
               <ul>
                  <li>Question: {{$user->question}}</li>
                  <li>Answer: {{$user->answer}}</li>
                  <li>By: {{$user->answered_by}}</li>
               </ul>
               <!-- </div> -->
               @endforeach

            </div>
         </div>

      </div>
   </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
