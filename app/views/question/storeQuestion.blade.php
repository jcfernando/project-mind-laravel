<!doctype html>
<html>
<head>
</head>
	<!--<link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">-->
		<!--<link rel="stylesheet" type="text/css" href="css/form.css">-->
	{{ HTML::style('css/bootstrap.min.css'); }}
	{{ HTML::style('css/form.css'); }}

<body>
	<!-- <nav class="navbar navbar-success"> -->
	  <!-- <div class="container-fluid">
	    <div class="navbar-header">
	      <a class="navbar-brand" href="#">
					{{ HTML::image('logo.png', 'mind-logo', array('class' => 'logo')) }}
	      </a>
	    </div>
	  </div> -->

	<div class="container">
		<div class="panel panel-success">
			<div class="panel-body">

				<div class="admin-tabs">
					<!-- Tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#pending" aria-control="pending" role="tab" data-toggle="tab">Pending</a></li>
						<li role="presentation"><a href="#answered" aria-control="answered" role="tab" data-toggle="tab">Answered</a></li>
					</ul>
				</div>

				<div class="tab-content">
					<div role="tabpanel" class="tab-pane fade in active" id="pending">
						<div class="row">
						<?php $i = 1; ?>	

						@if(isset($recordList))
							@foreach($recordList as $model)
								<div class="col-xs-6 col-md-4">
										<div class="question-container">

											<?php 
												$answertar = "#answer-modal-$i";
												$deletetar = "#delete-modal-$i";
												$i++;
											?> 

											<div class="question-buttons">
												<a href="#" data-toggle="modal" data-target="{{$answertar}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>	
												<a href="#" data-toggle="modal" data-target="{{$deletetar}}"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a>
											</div>

											<p class="question-p">
												{{$model['question']}}
											</p>

											<div class="row">
												<div class="col-xs-12 col-md-12">
													<p class="name-p">
														{{$model['name']}}
													</p>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-12 col-md-12">
													<div class="details-div">
														<p class="details-p">
															{{$model['school'], " | ", $model['contact'], " | ", $model['email']}}
														</p>
													</div>
												</div>
											</div>
										</div>
									</div>
							@endforeach
						@endif
						</div>
					</div>

					<div role="tabpanel" class="tab-pane fade" id="answered">
						Answered
					</div>
			</div>
		</div>
	</div>

	@for($j=1; $j<=$i; $j++)
	<?php
		$answerid = "answer-modal-$j";
		$deleteid = "delete-modal-$j";
	?>
	<div class="modal fade" id="{{$answerid}}" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        			<h4 class="modal-title">Answer Question</h4>
				</div>

				<div class="modal-body">
					{{$j}}
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="{{$deleteid}}" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<p>Are you sure you want to delete this question?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Delete</button>
	 			</div>
			</div>
		</div>
	</div>
	@endfor

	{{ HTML::script('js/jquery.min.js') }}
	{{ HTML::script('js/bootstrap.min.js') }}
	{{ HTML::script('js/grid.js') }}
	{{ HTML::script('js/store.js') }}



</body>
</html>
