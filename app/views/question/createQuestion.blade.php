<!doctype html>

<html>
<head>
	{{ HTML::style('css/bootstrap.min.css'); }}
	{{ HTML::style('css/form.css'); }}
</head>

<body>
	<nav class="navbar navbar-success">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <a class="navbar-brand" href="#">
					{{ HTML::image('logo.png', 'mind-logo', array('class' => 'logo')) }}
	      </a>
	    </div>
	  </div>
	</nav>

	<div class="container">
	<div class="panel panel-success">
			<div class="panel-body">
				{{ Form::open(array('route' => 'question.store', 'class' => 'form-horizontal'))}}

				<div class="form-group">
						<label for="name" class="col-sm-2 control-label">NAME</label>
						<div class="col-xs-12 col-sm-9 col-md-9" id="form_text">
							<input type="text" class="form-control name" name="name">
						</div>
				</div>

				<div class="form-group">
						<label for="email" class="col-sm-2 control-label">EMAIL:</label>
						<div class="col-xs-12 col-sm-9 col-md-9" id="form_text">
						   <input type="text" class="form-control email" name="email">
						</div>
				</div>

				<div class="form-group">
						<label for="contact" class="col-sm-2 control-label">CONTACT NUMBER:</label>
						<div class="col-xs-12 col-sm-9 col-md-9" id="form_text">
								<input type="text" class="form-control contact" name="contact">
						</div>
				</div>

				<div class="form-group">
						<label for="school" class="col-sm-2 control-label">SCHOOL:</label>
						<div class="col-xs-12 col-sm-9 col-md-9" id="form_text">
							<input type="text" class="form-control school" name="school">
						</div>
				</div>

				<div class="form-group">
				<label for="question" class="col-sm-2 control-label">SCIENCE QUESTION</label>
				<div class="col-xs-12 col-sm-9 col-md-9" id="form_text">
					<textarea class="form-control question" name="question" placeholder="Enter question here"></textarea>
				</div>
			  </div>

				<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-default btn-lg" id="Submit">SUBMIT</button>
				</div>
				</div>


				{{ Form::close() }}
			</div>
	</div>

	<div class="panel panel-success">
			<div class="panel-body">
			<h5 align="center"> Your questions will be answered during the Science Demo at <br>
				<br>11:00 AM
				<br>2:00 PM
				<br>5:00 PM<br>
				<br>The Science Demo will be at the Atom Gallery

			</h5>
		</div>
	</div>
	</div>




</body>
</html>
