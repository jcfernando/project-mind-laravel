<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|

Route::get('/', function()
{
	return View::make('question.createQuestion');
});
*/

Route::get('/', array('uses' => 'HomeController@create', 'as' => 'question.create'));
Route::post('question/store',  array('uses' =>  'HomeController@store', 'as' => 'question.store'));
Route::get('question/index', array('uses' => 'HomeController@index', 'as' => 'question.index'));
Route::get('windows', array('uses' => 'WindowsController@view', 'as' => 'windows.index'));
?>
